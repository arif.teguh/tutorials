extends KinematicBody2D

export (int) var speed = 400
export (int) var jump_speed = -600
export (int) var GRAVITY = 1200

const UP = Vector2(0,-1)

var velocity = Vector2()
var on_ground = false
var has_double_jumped = false

func get_input():
	velocity.x = 0
	if is_on_floor():
		on_ground = true
	else:
		on_ground = false

	if Input.is_action_just_pressed("up"):
		if on_ground == true:
			velocity.y = jump_speed 
			has_double_jumped = false
			on_ground = false
		elif on_ground == false and has_double_jumped == false:
        	velocity.y = jump_speed
        	has_double_jumped = true
	if Input.is_action_pressed('right'):
        velocity.x += speed
	if Input.is_action_pressed('left'):
        velocity.x -= speed


func _physics_process(delta):
    velocity.y += delta * GRAVITY
    get_input()
    velocity = move_and_slide(velocity, UP)
